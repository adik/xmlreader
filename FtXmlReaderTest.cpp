#include "FtXmlReader.h"
#include <gtest/gtest.h>

using namespace std::string_literals;

struct Foo
{
    int _i;
    char _c[200];
    std::string _s;

    friend FtXmlReader& operator>> (FtXmlReader& reader, Foo& f)
    {
        reader >> XmlAttributeTag("intAttr", f._i)
            >> XmlAttributeTag("cstr", f._c)
            >> XmlNodeValueTag(f._s);
        return reader;
    }
    Foo() : _i(0)
    {_c[0] = '\0';}
    bool operator == (const Foo& r) const
    {
        return _i == r._i &&
            _s == r._s &&
            strcmp(_c, r._c) == 0;
    }
    Foo(int i, const char* c, std::string&& s) : _i(i), _s(std::move(s))
    { strcpy(_c, c); }
    friend std::ostream& operator << (std::ostream& o, const Foo& f)
    {
        o<<"Foo: "<<f._i<<' ' <<f._c<<' '<<f._s;
        return o;
    }
};

struct Baz
{
    std::string _s;
    int _i;
    friend FtXmlReader& operator>> (FtXmlReader& reader, Baz& f)
    {
        reader >> XmlAttributeTag("str", f._s) >> XmlAttributeTag("int", f._i);
        return reader;
    }
    Baz() : _i(0)
    {}
    bool operator == (const Baz& z) const 
    {
        return _s == z._s &&
            _i == z._i;
    }
    Baz(std::string s, int i) : _s(std::move(s)), _i(i)
    {}
    friend std::ostream& operator<< (std::ostream& o, const Baz& z)
    {
        o<<"Baz: "<<z._s<<" "<<z._i;
        return o;
    }
};    

struct Bar
{
    int _a;
    double _b;
    std::vector<Foo> _f;
    Baz _z;
    friend FtXmlReader& operator>> (FtXmlReader& reader, Bar& f)
    {
        reader >> XmlAttributeTag("int", f._a)
            >> XmlAttributeTag("float", f._b)
            >> XmlNodeTag("foo", f._f)
            >> XmlNodeTag("baz", f._z);
        return reader;
    }
    Bar() : _a(0), _b(0)
    {}
    bool operator == (const Bar& b) const
    { 
        return _a == b._a &&
            _b == b._b &&
            _f ==  b._f &&
            _z == b._z;
    }
    Bar(int i, double d, std::vector<Foo>&& f, Baz&& z): _a(i), _b(d), _f(std::move(f)), _z(std::move(z))
    {}
    friend std::ostream& operator << (std::ostream& o, const Bar& b)
    { 
        o<<"Bar: "<<b._a<<' '<<b._b<<' ';
        for(auto f : b._f)
            o<<f<<' ';
        o<<b._z;
        return o;
    }
};

enum SomeMagicEnum
{
    kONE=10,
    kTWO=20,
    kUNKNOWN=-999
};

std::istream& operator>>(std::istream& in, SomeMagicEnum& e)
{
    std::string s;
    in>>s;
    if(s=="ONE")
        e= kONE;
    else if (s=="TWO")
        e=kTWO;
    else
        e=kUNKNOWN;
    return in;
};

struct SetOfEnumsClass
{
    std::set<SomeMagicEnum> _e;
    SetOfEnumsClass(std::set<SomeMagicEnum>&& s): _e(std::move(s)) {}
    SetOfEnumsClass(){}
    bool operator==(const SetOfEnumsClass& s) const { return _e ==s._e;}
    friend FtXmlReader& operator>> (FtXmlReader& reader, SetOfEnumsClass& s)
    {
        reader >> XmlNodeTag("enum", s._e);
        return reader;
    }
    friend std::ostream& operator << (std::ostream& o, const SetOfEnumsClass& e)
    {
        o<<"SetOfEnumsClass: ";
        for(auto x : e._e)
            o<<x<<' ';
        return o;
    }
};

struct SetOfEnumsClass2
{
    std::set<SomeMagicEnum> _e;
    SetOfEnumsClass2(std::set<SomeMagicEnum>&& s): _e(std::move(s)) {}
    SetOfEnumsClass2(){}
    bool operator==(const SetOfEnumsClass2& s) const { return _e ==s._e;}
    friend FtXmlReader& operator>> (FtXmlReader& reader, SetOfEnumsClass2& s)
    {
        reader >> XmlAttributeTag("enum", s._e, ",");
        return reader;
    }
    friend std::ostream& operator << (std::ostream& o, const SetOfEnumsClass2& e)
    {
        o<<"SetOfEnumsClass: ";
        for(auto x : e._e)
            o<<x<<' ';
        return o;
    }
};

struct SomeCfg
{
     std::string _s;
     int _i;
     bool operator==(const SomeCfg& c) const { return _s == c._s && _i == c._i; }
     friend FtXmlReader& operator>> (FtXmlReader& reader, SomeCfg& c)
     {
         return reader >> XmlAttributeTag("str", c._s) >> XmlAttributeTag("int", c._i);
     }
     friend std::ostream& operator << (std::ostream& o, const SomeCfg& c)
     {
         o <<"SomeCfg: " << c._s<< ' '<<c._i;
         return o;
     }
     SomeCfg() : _i(0) {}
     SomeCfg(std::string&& s, int i) : _s(std::move(s)), _i(i) {}
};
struct MapClass
{
     std::map<int, std::string> _m1;
     std::map<std::string, SomeCfg> _m2;
     std::multimap<std::string, int> _m3;
     bool operator==(const MapClass& m) const { return _m1 == m._m1 && _m2 == m._m2 && _m3 == m._m3; }
     friend FtXmlReader& operator>> (FtXmlReader& reader, MapClass& m)
     {
         return reader >> XmlNodeMapTag("map1", "key", "value", m._m1) >> 
                          XmlNodeMapTag("map2", "name", m._m2) >> 
                          XmlNodeMapTag("map3", "name", m._m3);
     }
     friend std::ostream& operator << (std::ostream& o, const MapClass& m)
     {
         o<<"MapClass: ";
         for(const auto& x : m._m1)
            o<<x.first<<":"<<x.second<<' ';
         for(const auto& x : m._m2)
            o<<x.first<<":"<<x.second<<' ';
         for (const auto& x : m._m3)
            o << x.first << ":" << x.second << ' ';
         return o;
     }
     MapClass(){}
     MapClass(std::map<int, std::string>&& m1, 
              std::map<std::string, SomeCfg>&& m2,
              std::multimap<std::string, int>&& m3) : _m1(std::move(m1)), _m2(std::move(m2)), _m3(std::move(m3)) {}
};



struct FtXmlReaderTest : public ::testing::Test
{ };

TEST_F(FtXmlReaderTest, Test_ParseXML_OneToOne)
{
    FtXmlReader reader;
    reader.ParseXmlString(R"(<nodes>
                    <bar int="111" float="34.56" >
                        <foo intAttr="1" cstr="AAA" >XXXXX</foo>
                        <foo intAttr="22" cstr="BBB" >YYYYYY</foo>
                        <foo intAttr="333" cstr="CCCC" >ZZZZZZ</foo>
                        <baz int="4321" str="ABC"/>
                    </bar>
                </nodes>)", "nodes");
    Bar b;
    reader >> XmlNodeTag("bar", b);
    Bar cmp = {111, 34.56, {{1,"AAA","XXXXX"},{22,"BBB","YYYYYY"},{333,"CCCC", "ZZZZZZ"}}, {"ABC", 4321}};
    EXPECT_EQ(cmp, b);
};

TEST_F(FtXmlReaderTest, Test_ParseXML_SetOfEnums)
{
    FtXmlReader reader;
    reader.ParseXmlString(R"(<enums>
                               <enum>ONE</enum>
                               <enum>TWO</enum>
                             </enums>)", "enums");
    SetOfEnumsClass s;
    reader >> s;
    SetOfEnumsClass cmp({kONE, kTWO});
    EXPECT_EQ(cmp, s);
};

TEST_F(FtXmlReaderTest, Test_ParseXML_SetOfEnumsTokenizedFromAttribute)
{
    FtXmlReader reader;
    reader.ParseXmlString(R"(<enums enum="ONE,TWO"/>)", "enums");
    SetOfEnumsClass2 s;
    reader >> s;
    SetOfEnumsClass2 cmp({kONE, kTWO});
    EXPECT_EQ(cmp, s);
};

TEST_F(FtXmlReaderTest, Test_ParseXML_Maps)
{
    FtXmlReader reader;
    reader.ParseXmlString(R"(<maps>
                               <map1 key="1" value="value1"/>
                               <map1 key="44" value="value44"/>
                               <map1 key="3311" value="value3311"/>
                               <map2 name="name1" str="cfg1" int="1"/>
                               <map2 name="name2" str="cfg2" int="2"/>
                               <map2 name="name1" str="cfg1" int="1"/>
                               <map3 name="name1">1</map3>
                               <map3 name="name1">2</map3>
                               <map3 name="name1">3</map3>
                               <map3 name="name2">4</map3>
                             </maps>)", "maps");
    MapClass m;
    reader>>m;
    MapClass cmp({{{1, "value1"},{44,"value44"},{3311,"value3311"}}, 
                 {{"name1",{"cfg1", 1}}, {"name2",{"cfg2", 2}}},
                 {{"name1", 1},{"name1", 2},{"name1", 3},{"name2",4}}});
    EXPECT_EQ(cmp,m);
}

TEST_F(FtXmlReaderTest, Test_FtStreetIoIMap)
{
    FtXmlReader reader;
    reader.ParseXmlString(R"(<brokers xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="BrokerNames.xsd">
                                <broker internal="BARC" external="Barclays" />
                                <broker internal="JPM" external="JPMorgan" />
                             </brokers>)", "brokers");

    std::map<std::string, std::string> m;
    reader >> XmlNodeMapTag("broker", "internal", "external", m);
    std::map<std::string, std::string> cmp{{"BARC", "Barclays"},{"JPM", "JPMorgan"}};
    EXPECT_EQ(cmp, m);
}