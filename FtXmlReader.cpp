#include "FtXmlReader.h"
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>

FtXmlReader& operator>>(FtXmlReader& reader, const XMLBeginNodeTag& x)
{
    if (!reader._nodesStack.empty())
    {
        xercesc::DOMElement* node = reader._nodesStack.top();
        if (node)
        {
            xercesc::DOMNodeList* nodeList = node->getElementsByTagName(XMLStringFromCStr(x._name));
            bool bFound = false;
            for (XMLSize_t i = 0; i < nodeList->getLength(); ++i)
            {
                auto node = nodeList->item(i);
                if (node->getNodeType() != xercesc::DOMNode::ELEMENT_NODE)
                    continue;
                if (!bFound)
                {
                    bFound = true;
                    reader._nodesStack.push(dynamic_cast<xercesc::DOMElement*>(node));
                }
                else
                {
                    terr << "Multiple nodes " << x._name << " found, will only use first" << tendl;
                    break;
                }
            }
            if (!bFound)
            {
                tinfo << "Node " << x._name << " not found" << tendl;
                reader._nodesStack.push(nullptr);
            }
        }
        else
        {
            // this should not happen, but just in case...
            reader._nodesStack.push(nullptr);
        }
    }
    return reader;
}

FtXmlReader& operator>>(FtXmlReader& reader, const XMLEndNodeTag& x)
{
    if (!reader._nodesStack.empty())
        reader._nodesStack.pop();
    return reader;
}

template<typename T>
xercesc::DOMDocument* XMLParser::ParseXMLIntoDoc(const T& t)
{
    _parser = std::make_unique< xercesc::XercesDOMParser>();
    _parser->useScanner(xercesc::XMLUni::fgSGXMLScanner);
    _parser->setValidationScheme(xercesc::XercesDOMParser::Val_Auto);
    _parser->setDoNamespaces(true);
    _parser->setErrorHandler(this);
    try
    {
        _parser->parse(t);
        return _parser->getDocument();
    }
    catch (const xercesc::XMLException& exc)
    {
        terr << "Parse exception: " << CStringFromXML(exc.getMessage()) << tendl;
    }
    catch (const xercesc::SAXParseException& exc)
    {
        terr << "Parse exception: " << CStringFromXML(exc.getMessage()) << tendl;
    }
    catch (...)
    {
        terr << "Unexpected Exception " << tendl;
    }
    _parser.reset();
    return nullptr;
}

XMLParser::XMLParser()
{
    try
    {
        xercesc::XMLPlatformUtils::Initialize();
    }
    catch (const xercesc::XMLException& exc)
    {
        std::cout << "XMLParser: Error during xerces initialization!: " << CStringFromXML(exc.getMessage()) << tendl;
        throw;
    }
}

XMLParser::~XMLParser()
{
    _parser.reset();
    try
    {
        xercesc::XMLPlatformUtils::Terminate();
    }
    catch (const xercesc::XMLException& exc)
    {
        terr << "XMLParser: Error during xerces termination!: " << CStringFromXML(exc.getMessage()) << tendl;
    }
}


void XMLParser::warning(const xercesc::SAXParseException& exc) 
{
    tinfo << "XML parser warning: " << CStringFromXML(exc.getSystemId()) << " at line: " << exc.getLineNumber() << " column: " << exc.getColumnNumber()
            << " message: " << CStringFromXML(exc.getMessage()) << tendl;
}

void XMLParser::error(const xercesc::SAXParseException& exc) 
{
    terr << "XML parser error: " << CStringFromXML(exc.getSystemId()) << " at line: " << exc.getLineNumber() << " column: " << exc.getColumnNumber()
            << " message: " << CStringFromXML(exc.getMessage()) << tendl;
}
    
void XMLParser::fatalError(const xercesc::SAXParseException& exc) 
{
    terr << "XML critical error: " << CStringFromXML(exc.getSystemId()) << " at line: " << exc.getLineNumber() << " column: " << exc.getColumnNumber()
        << " message: " << CStringFromXML(exc.getMessage()) << tendl;
}

FtXmlReader::FtXmlReader(const std::string& filePath)
{
    ParseFile(filePath);
}

FtXmlReader::FtXmlReader()
{}

CStringFromXML FtXmlReader::getAttributeValue(const std::string& name) const
{
    if (_nodesStack.empty())
        return {};
    const xercesc::DOMElement* node = _nodesStack.top();
    if(!node)
        return {};
    const xercesc::DOMAttr* attr = node->getAttributeNode(XMLStringFromCStr(name));
    if(!attr)
        return {};
    return CStringFromXML(attr->getValue());
}

CStringFromXML FtXmlReader::getNodeValue() const
{
    if (_nodesStack.empty())
        return {};
    const  xercesc::DOMElement* node = _nodesStack.top();
    if(!node)
        return {};
    return CStringFromXML(node->getTextContent());
}

bool FtXmlReader::ParseFile(const std::string& xmlFilePath)
{
    return ParseImpl(xmlFilePath.c_str());
}

bool FtXmlReader::ParseXmlString(const std::string& xml, const char* fakeId)
{
    return ParseImpl(xercesc::MemBufInputSource((const XMLByte*)xml.c_str(), xml.length(), fakeId, false));
}

template<typename T>
bool FtXmlReader::ParseImpl(const T& t)
{
    auto doc = _parser.ParseXMLIntoDoc(t);
    if (!doc)
        return false;
    auto root = doc->getDocumentElement();
    if (root)
        _nodesStack.push(root);
    return true;
}

