#pragma once

#include <string>
#include <vector>
#include <set>
#include <map>
#include <stack>
#include <memory>
#include "CEventLogger.h"
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/sax/AttributeList.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>
#include <optional>

/// xml node/attribute markers to be used in class >> operator, eg.:
/// class Foo
///  { int a; double b; Bar c; 
///  friend FtXmlReader& operator>> (FtXmlReader& reader, Foo& f)
///  { reader >> XmlAttributeTag("intAttribute", a) >> XmlAttributeTag("doubleAttribute", b) >> XmlNodeTag("bar", c);
///     return reader; }
/// ....  
/// Foo f;
/// FtXmlReader(pathToXml)>>XmlNodeTag("foo, f);
/// will populate struct Foo from xml:
/// <root><foo intAttribute="1" doubleAttribute="2.2"><bar ...></foo></root>

struct XMLBeginNodeTag 
{
    template<typename TStr>
    XMLBeginNodeTag(TStr&& name)  :_name(std::forward<TStr>(name)) {}
    const std::string _name;
};

struct XMLEndNodeTag 
{};

template<typename T>
struct XmlAttributeTag 
{
    template<typename TStr>
    XmlAttributeTag(TStr&& name, T& t, std::string&& tokenizeSep="") : _name(std::forward<TStr>(name)), _t(t), _separators(std::move(tokenizeSep)) {}
    const std::string _name;
    T& _t;
    const std::string _separators;
};

template<typename T>
struct XmlNodeValueTag 
{
    XmlNodeValueTag(T& t) : _t(t) {}
    T& _t;
};

template<typename T>
struct XmlNodeTag 
{
    template<typename TStr>
    XmlNodeTag(TStr&& name, T& t, std::string&& tokenizeSep="") : _name(std::forward<TStr>(name)), _t(t), _separators(std::move(tokenizeSep)) {}
    const std::string _name;
    T& _t;
    const std::string _separators;
};

template<typename TMap>
struct XmlNodeMapTag
{
    template<typename TStr1, typename TStr2>
    XmlNodeMapTag(TStr1&& name, TStr2&& keyAttrName, TMap& t) : _name(std::forward<TStr1>(name)), 
                                                                _keyAttrName(std::forward<TStr2>(keyAttrName)),
                                                                _t(t) {}
    template<typename TStr1, typename TStr2, typename TStr3>
    XmlNodeMapTag(TStr1&& name, TStr2&& keyAttrName, TStr3&& valAttrName, TMap& t) : _name(std::forward<TStr1>(name)),
                                                                                     _keyAttrName(std::forward<TStr2>(keyAttrName)),
                                                                                     _valueAttrName(std::forward<TStr3>(valAttrName)),
                                                                                     _t(t) {}
    const std::string _name;
    const std::string _keyAttrName;
    const std::optional<std::string> _valueAttrName;
    TMap& _t;
};


/// xerces wrappers

class CStringFromXML
{
    char* _string;
public:
    CStringFromXML(const CStringFromXML&) = delete;
    CStringFromXML& operator =(const CStringFromXML&) = delete;
    CStringFromXML(CStringFromXML&& r) noexcept(true)
    {
        _string = r._string;
        r._string = nullptr;
    }
    CStringFromXML& operator =(CStringFromXML&& r) noexcept(true)
    {
        if (_string)
            xercesc::XMLString::release(&_string);
        _string = r._string;
        r._string = nullptr;
        return *this;
    }
    CStringFromXML() : _string(nullptr)
    {}
    CStringFromXML(const XMLCh* const xmlText) : _string(xercesc::XMLString::transcode(xmlText))
    { }
    ~CStringFromXML()
    {
        if (_string)
            xercesc::XMLString::release(&_string);
    }
    inline operator const char* () const { return _string; }
    //operators
    friend std::ostream& operator << (std::ostream& o, const CStringFromXML& s)
    {
        o << (const char*)s;
        return o;
    }
    inline bool operator ==(const CStringFromXML& s) const noexcept(true)
    {
        return strcmp(s._string, _string) == 0;
    }
    inline bool operator ==(const XMLCh* s) const
    {
        return *this == CStringFromXML(s);
    }
    inline bool operator ==(const char* s) const noexcept(true)
    {
        return strcmp(s, _string) == 0;
    }
    inline bool operator ==(const std::string& s) const noexcept(true)
    {
        return s == _string;
    }

    friend bool operator ==(const XMLCh* l, const CStringFromXML& r) { return r == l; }
    friend bool operator ==(const char* l, const CStringFromXML& r) { return r == l; }
    friend bool operator ==(const std::string& l, const CStringFromXML& r) { return r == l; }
};

class XMLStringFromCStr
{
    XMLCh* _string;
public:
    XMLStringFromCStr(const XMLStringFromCStr&) = delete;
    XMLStringFromCStr& operator =(const XMLStringFromCStr&) = delete;
    XMLStringFromCStr(XMLStringFromCStr&& r) noexcept(true)
    {
        _string = r._string;
        r._string = nullptr;
    }
    XMLStringFromCStr& operator =(XMLStringFromCStr&& r) noexcept(true)
    {
        if (_string)
            xercesc::XMLString::release(&_string);
        _string = r._string;
        r._string = nullptr;
        return *this;
    }
    XMLStringFromCStr() : _string(nullptr)
    {}
    XMLStringFromCStr(const std::string& xmlText) : XMLStringFromCStr(xmlText.c_str())
    {}
    XMLStringFromCStr(const char* const xmlText) : _string(xercesc::XMLString::transcode(xmlText))
    { }
    ~XMLStringFromCStr()
    {
        if (_string)
            xercesc::XMLString::release(&_string);
    }
    inline operator const XMLCh* () const { return _string; }
    //operators
    friend std::ostream& operator << (std::ostream& o, const XMLStringFromCStr& s)
    {
        o << (const char*)CStringFromXML((const XMLCh*)s);
        return o;
    }
    inline bool operator ==(const XMLStringFromCStr& s) const
    {
        return xercesc::XMLString::equals(_string, s._string);
    }
    inline bool operator ==(const XMLCh* s) const
    {
        return xercesc::XMLString::equals(_string, s);
    }
    inline bool operator ==(const char* s) const noexcept(true)
    {
        return *this == XMLStringFromCStr(s);
    }
    inline bool operator ==(const std::string& s) const noexcept(true)
    {
        return *this == s.c_str();
    }

    friend bool operator ==(const XMLCh* l, const XMLStringFromCStr& r) { return r == l; }
    friend bool operator ==(const char* l, const XMLStringFromCStr& r) { return r == l; }
    friend bool operator ==(const std::string& l, const XMLStringFromCStr& r) { return r == l; }
};

class XMLAttributeListWrp
{
    xercesc::AttributeList& _attrs;
public:

    struct XMLAttributeNameAndValue
    {
        CStringFromXML name;
        CStringFromXML value;
    };
    template<int TMul>
    struct iteratorT
    {
        friend class XMLAttributeListWrp;
        using iterator_category = std::random_access_iterator_tag;
        using value_type = const XMLAttributeNameAndValue;
        using difference_type = std::ptrdiff_t;
        using pointer = const XMLAttributeNameAndValue*;
        using reference = const XMLAttributeNameAndValue&;
        iteratorT() : iteratorT(nullptr)
        {}
        iteratorT(const iteratorT&) = default;
        iteratorT(iteratorT&&) = default;
        iteratorT& operator = (const iteratorT&) = default;
        iteratorT& operator = (iteratorT&&) = default;
        bool operator == (const iteratorT& r) const noexcept(true) { return _attrs == r._attrs && _pos == r._pos; }
        bool operator != (const iteratorT& r) const noexcept(true) { return _attrs != r._attrs || _pos != r._pos; }
        reference operator *() const { return _attr; }
        pointer operator ->() const { return &_attr; }
        iteratorT& operator ++() { return advance(1); }
        iteratorT& operator --() { return advance(-1); }
        iteratorT& operator +=(int i) { return advance(i); }
        iteratorT& operator -=(int i) { return advance(-i); }
        iteratorT operator ++(int) { return postAdvance(1); }
        iteratorT operator --(int) { return postAdvance(-1); }
        iteratorT operator +(int i)
        {
            auto ret = *this;
            return ret.advance(i);
        }
        iteratorT operator -(int i)
        {
            auto ret = *this;
            return ret.advance(i);
        }
    private:
        iteratorT postAdvance(int off)
        {
            auto ret = *this;
            advance(off);
            return ret;
        }
        iteratorT& advance(int off)
        {
            if (_attrs)
            {
                _pos += TMul * off;
                if (_pos >= static_cast<int>(_attrs->getLength()) || _pos < 0)
                {
                    _attrs = nullptr;
                    _pos = 0;
                }
                else
                {
                    _attr = XMLAttributeNameAndValue{ _attrs->getName(_pos), _attrs->getValue(_pos) };
                }
            }
            return *this;
        }
        iteratorT(xercesc::AttributeList* attrs, int pos = 0) : _attrs(attrs), _pos((TMul < 0 && attrs) ? (attrs->getLength() - 1 - pos) : pos)
        {
            advance(0);
        }
        xercesc::AttributeList* _attrs;
        XMLAttributeNameAndValue  _attr;
        int _pos;
    };
    XMLAttributeListWrp(xercesc::AttributeList& attrs) : _attrs(attrs)
    {}
    using iterator = iteratorT<1>;
    using reverse_iterator = iteratorT<-1>;
    iterator begin() const { return iterator(&_attrs); }
    iterator end() const { return iterator(); }
    iterator cbegin() const { return iterator(&_attrs); }
    iterator cend() const { return iterator(); }
    reverse_iterator rbegin() const { return reverse_iterator(&_attrs); }
    reverse_iterator rend() const { return reverse_iterator(); }
    reverse_iterator crbegin() const { return reverse_iterator(&_attrs); }
    reverse_iterator crend() const { return reverse_iterator(); }
    iterator find(const char* name) const
    {
        const int noAttrs = _attrs.getLength();
        for (int i = 0; i < noAttrs; ++i)
            if (CStringFromXML(_attrs.getName(i)) == name)
                return iterator(&_attrs, i);
        return iterator();
    }
    iterator find(const std::string& name) const { return find(name.c_str()); }
};

struct XMLParser : public xercesc::HandlerBase
{
    template<typename T>
    xercesc::DOMDocument* ParseXMLIntoDoc(const T& t);

    XMLParser();
    ~XMLParser();
    void warning(const xercesc::SAXParseException& exc) override;
    void error(const xercesc::SAXParseException& exc) override;
    void fatalError(const xercesc::SAXParseException& exc) override;
private:
    std::unique_ptr<xercesc::XercesDOMParser> _parser;
};
/// reader class

struct FtXmlReader 
{
    friend FtXmlReader& operator>>(FtXmlReader& reader, const XMLBeginNodeTag& x);
    friend FtXmlReader& operator>>(FtXmlReader& reader, const XMLEndNodeTag& x);
    // specialize for char array
    template<size_t N>
    friend FtXmlReader& operator>>(FtXmlReader& reader, const XmlAttributeTag<char[N]>& x)
    {
        const CStringFromXML value = reader.getAttributeValue(x._name);
        if(value)
            strlcpy(x._t, (const char*)value, N);
        return reader;
    }
    template<typename T> 
    friend FtXmlReader& operator>>(FtXmlReader& reader, const XmlAttributeTag<T>& x)
    {
        const CStringFromXML value = reader.getAttributeValue(x._name);
        if(value)
        {
            try
            {
                 x._t = boost::lexical_cast<T>((const char*)value);
            }
            catch (const std::bad_cast&)
            {
                 terr << "FtXmlReadre: Failed to cast " << value << " to " << typeid(T).name() << " in attribute " << x._name << tendl;
            }
	}
        return reader;
    }
    template<typename T, typename Allocator>
    friend FtXmlReader& operator>>(FtXmlReader& reader, const XmlAttributeTag<std::vector<T,Allocator>>& x)
    {
         const CStringFromXML value = reader.getAttributeValue(x._name);
         if(value)
             reader.TokenizeTo((const char*)value, x._separators, x._t, std::back_inserter(x._t));
         return reader;
    }
    template<typename T, typename TLess, typename Allocator>
    friend FtXmlReader& operator>>(FtXmlReader& reader, const XmlAttributeTag<std::set<T,TLess, Allocator>>& x)
    {
         const CStringFromXML value = reader.getAttributeValue(x._name);
         if(value)
             reader.TokenizeTo((const char*)value, x._separators, x._t, std::inserter(x._t, x._t.begin()));
         return reader;
    }       
    
    template<size_t N>
    friend FtXmlReader& operator>>(FtXmlReader& reader, const XmlNodeValueTag<char[N]>& x)
    {
        const CStringFromXML value = reader.getNodeValue(x._name);
        if(value)
            strlcpy(x._value, value, N);
        return reader;
    }
    template<typename T>
    friend FtXmlReader& operator>>(FtXmlReader& reader, const XmlNodeValueTag<T>& x)
    {
        const CStringFromXML value = reader.getNodeValue();
        if(value)
        {
            try
            {
                 x._t = boost::lexical_cast<T>((const char*)value);
            }
            catch (const std::bad_cast&)
            {
                 terr << "FtXmlReadre: Failed to cast " << value << " to " << typeid(T).name() << " in node " << 
                          CStringFromXML(reader._nodesStack.top()->getLocalName()) << tendl;
            }
        }
        return reader;
    }
    template<typename T, typename Allocator>
    friend FtXmlReader& operator>>(FtXmlReader& reader, const XmlNodeValueTag<std::vector<T, Allocator>>& x)
    {
         const CStringFromXML value = reader.getNodeValue();
         if(value)
             TokenizeTo(value, x._separators, x._value, std::back_inserter(x._value));
         return reader;
    }
    template<typename T, typename TLess, typename Allocator>
    friend FtXmlReader& operator>>(FtXmlReader& reader, const XmlNodeValueTag<std::set<T,TLess, Allocator>>& x)
    {
         const CStringFromXML value = reader.getNodeValue();
         if(value)
             TokenizeTo(value, x._separators, x._value, std::inserter(x._value));
         return reader;
    }

    template<typename T>
    friend FtXmlReader& operator>>(FtXmlReader& reader, const XmlNodeTag<T>& x)
    {
        reader >> XMLBeginNodeTag(x._name) >> x._t >> XMLEndNodeTag();
        return reader;
    }
    template<typename T, typename Allocator>
    friend FtXmlReader& operator>>(FtXmlReader& reader, const XmlNodeTag<std::vector<T, Allocator>>& x)
    {
        reader.processContainerOf(x, x._t, std::back_inserter(x._t));
        return reader;
    }
    template<typename T, typename TLess, typename Allocator>
    friend FtXmlReader& operator>>(FtXmlReader& reader, const XmlNodeTag<std::set<T, TLess, Allocator>>& x)
    {
        reader.processContainerOf(x, x._t, std::inserter(x._t, x._t.begin()));
        return reader;
    }
    template<typename T>
    friend FtXmlReader& operator>>(FtXmlReader& reader, T& t)
    {
        reader >> XmlNodeValueTag(t);
        return reader;
    }

private:
    template<typename T, typename U=void>
    struct is_boost_lexcastable : std::false_type{};
    template<typename T>
    struct is_boost_lexcastable<T, decltype(void(std::declval<std::istream&>() >> std::declval<T&>()))> : std::true_type {};

    template<template<typename,typename,typename,typename>class TMap, typename TKey, typename T, typename TLess, typename TAllocator>
    typename std::enable_if<is_boost_lexcastable<T>::value>::type
    readAsAttibute(const XmlNodeMapTag<TMap<TKey, T, TLess, TAllocator>>& x, T& t)
    {  *this >> XmlAttributeTag(*x._valueAttrName, t);  }

    template<template<typename,typename,typename,typename>class TMap, typename TKey, typename T, typename TLess, typename TAllocator>
    typename std::enable_if<!is_boost_lexcastable<T>::value>::type
    readAsAttibute(const XmlNodeMapTag<TMap<TKey, T, TLess, TAllocator>>& x, T& t)
    {  terr <<  *x._valueAttrName << "is not boost::laxical_cast convertible" << tendl;  }
public:   

    template<template<typename,typename,typename,typename>class TMap, typename TKey, typename T, typename TLess, typename TAllocator>
    friend FtXmlReader& operator>>(FtXmlReader& reader, const XmlNodeMapTag<TMap<TKey, T, TLess, TAllocator>>& x)
    {
        if (reader._nodesStack.empty())
            return reader;

        const xercesc::DOMElement* node = reader._nodesStack.top();
        if (!node)
            return reader;

        const xercesc::DOMNodeList* nodeList = node->getElementsByTagName(XMLStringFromCStr(x._name));
        for (XMLSize_t i = 0; i < nodeList->getLength(); ++i)
        {
            const auto node = nodeList->item(i);
            if (node->getNodeType() != xercesc::DOMNode::ELEMENT_NODE)
                continue;

            reader._nodesStack.push(dynamic_cast<xercesc::DOMElement*>(node));
            const CStringFromXML keyAttr = reader.getAttributeValue(x._keyAttrName);
            if(!keyAttr)
            {
                terr << "FtXmlReade: Missing key attribute: " << x._keyAttrName << ", ignoring tag: " << x._name << tendl;
            }
            else
            {
                try
                {
                    const TKey key = boost::lexical_cast<TKey>(keyAttr);
                    T t;
                    if (x._valueAttrName)
                        reader.readAsAttibute(x, t);
                    else
                        reader >> t;
                    reader.insertIntoMap(key, std::move(t), x);
                }
                catch(const std::bad_cast&)
                {
                    terr << "FtXmlReader: Failed to convert maps key " << keyAttr << " to " << typeid(TKey).name() << tendl;
                }
            }
            reader._nodesStack.pop();
        }
        return reader;
    }

    FtXmlReader(const std::string& filePath);
    FtXmlReader();

    bool ParseFile(const std::string& xmlFilePath);
    bool ParseXmlString(const std::string& xml, const char* fakeId);
private:
    CStringFromXML getAttributeValue(const std::string& name) const;
    CStringFromXML getNodeValue() const;

    template<typename T>
    bool ParseImpl(const T& t);

    template<template<typename, typename...> class TContainer, typename T, typename ...ContParams, typename TInserter>
    void TokenizeTo(const std::string& str, const std::string& separators, TContainer<T, ContParams...>& c, TInserter&& ins)
    {
        try
        {
            typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
            boost::char_separator<char> sep(separators.c_str());
            tokenizer tokens(str, sep);
            std::transform(tokens.begin(), tokens.end(), ins, [](const std::string& s) { return boost::lexical_cast<T>(s); });
        }
        catch (const std::bad_cast&)
        {
            terr << "FtXmlReadre: Failed to tokenize and cast " << str << " to " << typeid(T).name() << tendl;
        }
    }

    template<typename TXmlNode, template<typename, typename...> class TContainer, typename T, typename ...ContParams, typename TInserter>
    void processContainerOf(const TXmlNode& x, TContainer<T, ContParams...>& c, TInserter&& ins)
    {
        if (_nodesStack.empty())
            return;

        const xercesc::DOMElement* node = _nodesStack.top();
        if (!node)
            return;

        const xercesc::DOMNodeList* nodeList = node->getElementsByTagName(XMLStringFromCStr(x._name));
        for (XMLSize_t i = 0; i < nodeList->getLength(); ++i)
        {
            const auto node = nodeList->item(i);
            if (node->getNodeType() != xercesc::DOMNode::ELEMENT_NODE)
                continue;

            _nodesStack.push(dynamic_cast<xercesc::DOMElement*>(node));
            T t;
            (*this)>>t;
            ins=std::move(t);
            _nodesStack.pop();
        }
    }

    template<template<typename, typename, typename, typename>class TMap, typename TKey, typename T, typename TLess, typename Allocator>
    void insertIntoMap(const TKey& key, T&& t, const XmlNodeMapTag<TMap<TKey, T, TLess, Allocator>>& x)
    {
        if (!x._t.emplace(key, std::move(t)).second)
            terr << "FtXmlReader: Duplicate key detected: " << key << " in: " << x._name << tendl;
    }
    template<typename TKey, typename T, typename TLess, typename Allocator>
    void insertIntoMap(const TKey& key, T&& t, const XmlNodeMapTag<std::multimap<TKey, T, TLess, Allocator>>& x)
    {
        x._t.emplace(key, std::move(t));
    }

    std::stack<xercesc::DOMElement*> _nodesStack;
    XMLParser _parser;
};
